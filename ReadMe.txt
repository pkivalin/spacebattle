Exercise:

Develop a vertical scrolling shooter game:

● Minimum required game elements:
player's ship
opponent's ship(s)
bullet(s)

● The HUD should display the score and the remaining lifes
● The player's ship can be either controlled via keyboard controls or via the mouse
● It should be possible to pause the game
● Implement a game over dialog with the possibility to play the game again
● Extra (not mandatory): Save and show high scores
● Extra (not mandatory): Implement a scrolling star background
● Extra (not mandatory): Provide a diagram (e.g. UML) explaining your concept