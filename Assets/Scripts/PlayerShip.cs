﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerShip : AShip
{
    static private Vector2 X_BOUNDS = new Vector2(-260, 260);
    static private Vector2 Y_BOUNDS = new Vector2(-440, 440);

    [SerializeField]
    private Button fireBtn;
    [SerializeField]
    private LivesPanel livesPanel;

    private ACharacterController keyboardController;
    private ACharacterController mouseController;
    private ACharacterController curController;
    private Vector3 delaultPosition;
    private bool alive = true;

    override protected void Start()
    {
        base.Start();
        delaultPosition = transform.localPosition;
        bulletsPool = GetComponent<ObjectPool>();
        fireBtn.onClick.AddListener(Shot);
        mouseController = new MouseCharacterController(transform, speed, X_BOUNDS, Y_BOUNDS);
        keyboardController = new KeyboardCharacterController(transform, speed, X_BOUNDS, Y_BOUNDS);
        EventDispatcher.startGameEvent += StartGame;
        EventDispatcher.onSwitchCharacterControllerEvent += SwitchController;
        curController = keyboardController;
    }

    private void SwitchController(bool isKeyboardChoosen)
    {
        curController = isKeyboardChoosen ? keyboardController : mouseController;
    }
    
    protected override void Update()
    {
        base.Update();
        if (Input.GetKeyUp(KeyCode.Space))
            Shot();
    }

    protected void FixedUpdate()
    {
        if (alive)
            curController.Move();
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "BonusLive")
        {
            curLivesCnt = Mathf.Min(livesCnt, ++curLivesCnt);
            livesPanel.curLivesCnt = curLivesCnt;
        }
        else if (other.name != "Boundary")
        {
            if (--curLivesCnt == 0)
            {
                Deactivate();
                ShowExplosion();
                Invoke("GameOver", Explosion.DURATION);
            }
            livesPanel.curLivesCnt = curLivesCnt;
        }
    }

    private void GameOver()
    {
        EventDispatcher.instance.GameOver();
    }

    private void StartGame()
    {
        CancelInvoke();
        transform.localPosition = delaultPosition;
        curLivesCnt = livesCnt;
        livesPanel.curLivesCnt = curLivesCnt;
        gameObject.SetActive(true);
    }
}
