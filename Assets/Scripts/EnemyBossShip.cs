﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyBossShip : AShip
{

    static private Vector2 X_BOUNDS = new Vector2(-250, 250);
    static private Vector2 START_POSITION = new Vector2(350, 390);

    [SerializeField]
    protected int points;
    [SerializeField]
    public float spawnDelay;

    private bool facingLeft;

    override protected void Start()
    {
        base.Start();
        bulletsPool = GameObject.Find("EnemyBulletsPool").GetComponent<ObjectPool>();
    }

    protected override void Update()
    {
        base.Update();
        Shot();
        if ((facingLeft && transform.localPosition.x < X_BOUNDS.x)
        || (!facingLeft && transform.localPosition.x > X_BOUNDS.y))
            ChangeDirection();
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name != "Boundary")
        {
            if (--curLivesCnt == 0)
            {
                EventDispatcher.instance.OnBossKilled(points);
                Deactivate();
                ShowExplosion();
            }
        }
    }

    public void Spawn()
    {
        facingLeft = Random.Range(0, 2) == 1;
        curLivesCnt = livesCnt;
        gameObject.SetActive(true);
        transform.localPosition = new Vector3(facingLeft ? START_POSITION.x : - START_POSITION.x, START_POSITION.y);
        GetComponent<Rigidbody2D>().velocity = new Vector2(facingLeft ? -speed : speed, 0);
    }

    private void ChangeDirection()
    {
        facingLeft = !facingLeft;
        GetComponent<Rigidbody2D>().velocity = new Vector2(facingLeft ? -speed : speed, 0);
    }
}
