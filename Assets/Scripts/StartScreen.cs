﻿using UnityEngine;
using System.Collections;

public class StartScreen : AScreen
{

    override protected void AddListeners()
    {
        EventDispatcher.startGameEvent += Hide;
    }

}
