﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class EventDispatcher : MonoBehaviour
{
    static public EventDispatcher instance;

    public delegate void GameOverEvent();
    public static event GameOverEvent gameOverEvent;

    public delegate void StartGameEvent();
    public static event StartGameEvent startGameEvent;

    public delegate void PauseGameEvent();
    public static event PauseGameEvent pauseGameEvent;

    public delegate void ResumeGameEvent();
    public static event ResumeGameEvent resumeGameEvent;

    public delegate void OnEnemyKilledEvent(int points);
    public static event OnEnemyKilledEvent onEnemyKilledEvent;

    public delegate void OnBossKilledEvent(int points);
    public static event OnBossKilledEvent onBossKilledEvent;

    public delegate void OnSwitchCharacterControllerEvent(bool isKeyboardCharacterController);
    public static event OnSwitchCharacterControllerEvent onSwitchCharacterControllerEvent;

    void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        startGameEvent();
    }

    public void GameOver()
    {
        gameOverEvent();
    }

    public void PauseGame()
    {
        pauseGameEvent();
    }

    public void ResumeGame()
    {
        resumeGameEvent();
    }

    public void OnEnemyKilled(int points)
    {
        onEnemyKilledEvent(points);
    }

    public void OnBossKilled(int points)
    {
        onBossKilledEvent(points);
    }

    public void OnSwitchCharacterController(bool isKeyboardCharacterController)
    {
        onSwitchCharacterControllerEvent(isKeyboardCharacterController);
    }

}