﻿using UnityEngine;
using System.Collections;

public class GameOverScreen : AScreen
{

    override protected void AddListeners()
    {
        EventDispatcher.gameOverEvent += Show;
        EventDispatcher.startGameEvent += Hide;
        Hide();
    }

}
