﻿using UnityEngine;
using System.Collections;

public class Bullet : ABoundaryChecker
{

    protected void Start()
    {
        EventDispatcher.startGameEvent += Deactivate;
    }

    public void Spawn(Vector3 startPosition, Vector2 direction)
    {
        gameObject.SetActive(true);
        transform.position = startPosition;
        GetComponent<Rigidbody2D>().velocity = direction;
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name != "Boundary")
            Deactivate();
    }
}
