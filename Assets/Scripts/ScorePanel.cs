﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScorePanel : MonoBehaviour
{

    private const string BEST_SCORE_KEY = "bestScore";

    [SerializeField]
    private Text scoreTF;
    [SerializeField]
    private Text bestScoreTF;
    private int _score;
    private int score
    {
        set
        {
            _score = value;
            scoreTF.text = _score.ToString();
        }
        get
        {
            return _score;
        }
    }

    private int _bestScore;
    private int bestScore
    {
        set
        {
            _bestScore = value;
            bestScoreTF.text = _bestScore.ToString();
        }
        get
        {
            return _bestScore;
        }
    }

    void Start()
    {
        EventDispatcher.startGameEvent += StartGame;
        EventDispatcher.gameOverEvent += GameOver;
        EventDispatcher.onBossKilledEvent += AddEnemyPoints;
        EventDispatcher.onEnemyKilledEvent += AddEnemyPoints;
        StartGame();
    }

    private void StartGame()
    {
        bestScore = PlayerPrefs.GetInt(BEST_SCORE_KEY, 0);
        score = 0;
    }

    private void GameOver()
    {
        if (score > bestScore)
        {
            bestScore = score;
            PlayerPrefs.SetInt(BEST_SCORE_KEY, bestScore);
        }
    }

    private void AddEnemyPoints(int points)
    {
        score += points;
    }

}
