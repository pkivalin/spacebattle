﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour
{
    private List<GameObject> pool;
    [SerializeField]
    private GameObject pooledGameObject;

    protected void Start()
    {
        pool = new List<GameObject>();
    }

    public GameObject GetPooledObject(Transform parentTransform)
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if (!pool[i].activeSelf)
                return pool[i];
        }

        GameObject pooledObject = Instantiate(pooledGameObject);
        pooledObject.transform.SetParent(parentTransform);
        pool.Add(pooledObject);
        return pooledObject;
    }
}
