﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

abstract public class AShip : ABoundaryChecker
{

    [SerializeField]
    protected float speed;
    [SerializeField]
    private float bulletSpeed;
    [SerializeField]
    private float shotDelay;
    [SerializeField]
    private float bulletShiftY;
    [SerializeField]
    protected int livesCnt;
    protected int curLivesCnt;
    private float nextShotDelay;
    protected ObjectPool bulletsPool;
    protected ObjectPool explosionsPool;
    private Vector3 bulletShift;

    private bool onPause = false;

    virtual protected void Start()
    {
        EventDispatcher.pauseGameEvent += Pause;
        EventDispatcher.resumeGameEvent += Resume;
        EventDispatcher.startGameEvent += Resume;
        EventDispatcher.startGameEvent += Deactivate;
        bulletShift = new Vector3(0, bulletShiftY, 0);
        explosionsPool = GameObject.Find("ExplosionsPool").GetComponent<ObjectPool>();
    }

    private void Pause()
    {
        onPause = true;
    }

    private void Resume()
    {
        onPause = false;
    }

    protected void Shot()
    {
        if (!CanShot())
            return;
        Bullet bullet = bulletsPool.GetPooledObject(transform.parent).GetComponent<Bullet>();
        bullet.Spawn(transform.position + bulletShift, new Vector3(0, bulletSpeed, 0));
        nextShotDelay = shotDelay;
    }

    protected bool CanShot()
    {
        return !onPause && nextShotDelay <= 0;
    }

    virtual protected void Update()
    {
        nextShotDelay -= Time.deltaTime;
    }

    protected void ShowExplosion()
    {
        Explosion explosion = explosionsPool.GetPooledObject(transform.parent).GetComponent<Explosion>();
        explosion.Spawn(transform.position);
    }

}
