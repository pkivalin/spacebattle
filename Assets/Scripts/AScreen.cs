﻿using UnityEngine;
using System.Collections;

abstract public class AScreen : MonoBehaviour
{

    protected void Start()
    {
        AddListeners();
    }

    abstract protected void AddListeners();

    protected void Show()
    {
        gameObject.SetActive(true);
    }

    protected void Hide()
    {
        gameObject.SetActive(false);
    }
}
