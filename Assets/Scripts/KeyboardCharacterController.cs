﻿using UnityEngine;
using System;

public class KeyboardCharacterController : ACharacterController
{

    private float diagonalSpeed;

    public KeyboardCharacterController(Transform characterTransform, float speed, Vector2 xBounds, Vector2 yBounds)
        : base(characterTransform, speed, xBounds, yBounds)
    {
        diagonalSpeed = Mathf.Sqrt(speed * speed / 2f);
    }

    public override void Move()
    {
        float deltaX = 0;
        float deltaY = 0;
        float moveX = Input.GetAxis("Horizontal");
        float moveY = Input.GetAxis("Vertical");

        if (moveX != 0 && moveY != 0)
            deltaX = deltaY = diagonalSpeed;
        else if (moveX != 0)
            deltaX = speed;
        else if (moveY != 0)
            deltaY = speed;

        deltaX *= moveX < 0 ? -1 : 1;
        deltaY *= moveY < 0 ? -1 : 1;

        CheckBoundsAndUpdatePosition(deltaX, deltaY);
    }
}
