﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LivesPanel : MonoBehaviour
{

    [SerializeField]
    private Sprite liveEmptySprite;
    [SerializeField]
    private Sprite liveFullSprite;
    [SerializeField]
    private Image[] lives;
    private int _curLivesCnt;
    public int curLivesCnt
    {
        set
        {
            _curLivesCnt = value;
            for (int i = 0; i < lives.Length; i++)
                lives[i].sprite = i < _curLivesCnt ? liveFullSprite : liveEmptySprite;
        }
    }

}
