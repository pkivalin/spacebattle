﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BonusLive : ABoundaryChecker
{

    static private Vector2 X_BOUNDS = new Vector2(-270, 270);
    private const float START_Y = 510;

    [SerializeField]
    protected float speed;
    [SerializeField]
    public float spawnDelay;
    protected ObjectPool explosionsPool;

    protected void Start()
    {
        EventDispatcher.startGameEvent += Deactivate;
        explosionsPool = GameObject.Find("ExplosionsPool").GetComponent<ObjectPool>();
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name != "Boundary")
        {
            Deactivate();
            if (other.name != "PlayerShip")
                ShowExplosion();
        }
    }

    protected void ShowExplosion()
    {
        Explosion explosion = explosionsPool.GetPooledObject(transform.parent).GetComponent<Explosion>();
        explosion.Spawn(transform.position);
    }

    public void Spawn()
    {
        gameObject.SetActive(true);
        transform.localPosition = new Vector3(Random.Range(X_BOUNDS.x, X_BOUNDS.y), START_Y);
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
    }
}
