﻿using UnityEngine;
using System.Collections;

public class PauseScreen : AScreen
{

    override protected void AddListeners()
    {
        EventDispatcher.pauseGameEvent += Show;
        EventDispatcher.resumeGameEvent += Hide;
        EventDispatcher.startGameEvent += Hide;
        Hide();
    }

}
