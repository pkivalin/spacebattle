﻿using UnityEngine;
using System;
using System.Collections.Generic;

abstract public class ACharacterController
{
    

    protected Transform characterTransform;
    protected float speed;
    private Vector2 xBounds;
    private Vector2 yBounds;

    public ACharacterController(Transform characterTransform, float speed, Vector2 xBounds, Vector2 yBounds)
    {
        this.characterTransform = characterTransform;
        this.speed = speed;
        this.xBounds = xBounds;
        this.yBounds = yBounds;
    }

    abstract public void Move();

    protected void CheckBoundsAndUpdatePosition(float deltaX, float deltaY)
    {
        float newX = characterTransform.localPosition.x + deltaX;
        newX = Mathf.Clamp(newX, xBounds.x, xBounds.y);

        float newY = characterTransform.localPosition.y + deltaY;
        newY = Mathf.Clamp(newY, yBounds.x, yBounds.y);

        characterTransform.localPosition = new Vector3(newX, newY);
    }
}
