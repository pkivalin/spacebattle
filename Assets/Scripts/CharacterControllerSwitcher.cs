﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterControllerSwitcher : MonoBehaviour
{

    [SerializeField]
    private Toggle keyboardToggle;
    [SerializeField]
    private Toggle mouseToggle;

    protected void Awake()
    {
        EventDispatcher.onSwitchCharacterControllerEvent += OnSwitch;
        keyboardToggle.onValueChanged.AddListener(Switch);
    }

    private void OnSwitch(bool isKeyboard)
    {
        keyboardToggle.isOn = isKeyboard;
        mouseToggle.isOn = !isKeyboard;
    }

    private void Switch(bool isKeyboard)
    {
        if (gameObject.activeInHierarchy)
            EventDispatcher.instance.OnSwitchCharacterController(isKeyboard);
    }

}
