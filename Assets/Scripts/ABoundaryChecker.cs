﻿using UnityEngine;
using System;
using System.Collections.Generic;

abstract public class ABoundaryChecker : MonoBehaviour
{

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "Boundary")
            Deactivate();
    }

    protected void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
