﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour
{

    [SerializeField]
    private ObjectPool enemyShipsPool;
    [SerializeField]
    private float spawnEnemyDelay;
    [SerializeField]
    private EnemyBossShip enemyBossShip;
    [SerializeField]
    private BonusLive bonusLive;

    protected void Start()
    {
        Time.timeScale = 0;
        EventDispatcher.pauseGameEvent += Pause;
        EventDispatcher.resumeGameEvent += Resume;
        EventDispatcher.startGameEvent += StartGame;
        EventDispatcher.gameOverEvent += GameOver;
        EventDispatcher.onBossKilledEvent += OnBossKilled;
    }

    private void StartGame()
    {
        Time.timeScale = 1;
        InvokeRepeating("SpawnEnemy", spawnEnemyDelay, spawnEnemyDelay);
        InvokeRepeating("SpawnBonusLive", bonusLive.spawnDelay, bonusLive.spawnDelay);
        Invoke("SpawnEnemyBoss", enemyBossShip.spawnDelay);
    }

    private void GameOver()
    {
        Time.timeScale = 0;
        CancelInvoke();
    }

    private void Pause()
    {
        Time.timeScale = 0;
    }

    private void Resume()
    {
        Time.timeScale = 1;
    }

    private void OnBossKilled(int points)
    {
        Invoke("SpawnEnemyBoss", enemyBossShip.spawnDelay);
    }

    private void SpawnEnemyBoss()
    {
        enemyBossShip.Spawn();
    }

    private void SpawnBonusLive()
    {
        bonusLive.Spawn();
    }

    private void SpawnEnemy()
    {
        EnemyShip enemyShip = enemyShipsPool.GetPooledObject(transform).GetComponent<EnemyShip>();
        enemyShip.Spawn();
    }

}
