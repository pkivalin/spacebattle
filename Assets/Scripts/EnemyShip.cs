﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyShip : AShip
{

    static private Vector2 X_BOUNDS = new Vector2(-270, 270);
    private const float START_Y = 510;

    [SerializeField]
    protected int points;

    override protected void Start()
    {
        AssetBundle bundle = new AssetBundle();
        base.Start();
        EventDispatcher.startGameEvent += Deactivate;
        bulletsPool = GameObject.Find("EnemyBulletsPool").GetComponent<ObjectPool>();
    }

    protected override void Update()
    {
        base.Update();
        Shot();
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name != "Boundary")
        {
            EventDispatcher.instance.OnEnemyKilled(points);
            ShowExplosion();
            Deactivate();
        }
    }

    public void Spawn()
    {
        gameObject.SetActive(true);
        transform.localPosition = new Vector3(Random.Range(X_BOUNDS.x, X_BOUNDS.y), START_Y);
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
    }
}
