﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{

    public const float DURATION = 0.25f;

    protected void Start()
    {
        EventDispatcher.startGameEvent += Deactivate;
    }

    public void Spawn(Vector3 startPosition)
    {
        gameObject.SetActive(true);
        transform.position = startPosition;
        CancelInvoke();
        Invoke("Deactivate", DURATION);
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }

}
