﻿using UnityEngine;
using System;

public class MouseCharacterController : ACharacterController
{

    public MouseCharacterController(Transform characterTransform, float speed, Vector2 xBounds, Vector2 yBounds)
        : base(characterTransform, speed, xBounds, yBounds)
    {
    }

    public override void Move()
    {
        float deltaX = 0;
        float deltaY = 0;
        Vector3 worldCursorPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 direction = worldCursorPosition - characterTransform.position;
        direction.z = 0;
        Vector3 distance = direction.normalized * speed;
        if (distance.magnitude > direction.magnitude)
            distance = direction;
        deltaX = distance.x;
        deltaY = distance.y;

        CheckBoundsAndUpdatePosition(deltaX, deltaY);
    }
}
